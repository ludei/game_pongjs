/**
* Author: Iker Jamardo Zugaza
* Company: Ludei (www.ludei.com)
* Disclaimer: 
    Please, note that the purpose of this software is merely educational. Plenty
    of error checking should be added to make it for professional use.
* License: LGPL v3
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* =====================
* Pong.Paddle
* =====================
*/
Pong.Paddle = function() {
};

Pong.extend(Pong.Paddle, Pong.MovingRectangle);

Pong.Paddle.prototype.score = 0;
Pong.Paddle.prototype.scoreX = 0;
Pong.Paddle.prototype.scoreY = 0;

/**
* =====================
* Pong.Ball
* =====================
*/
Pong.Ball = function() {
};

Pong.extend(Pong.Ball, Pong.MovingRectangle);

/**
* =====================
* Pong.Game
* =====================
*/
Pong.Game = function() {
};

Pong.Game.prototype = {
    timer : null,
    fpsCounter : null,
    input : null,
    renderer : null,
    gameSpaceToRenderSpaceConversionFactor : 1,
    rightWall : null,
    leftWall : null,
    topWall : null,
    bottomWall : null,
    paddleLeft : null,
    paddleRight : null,
    paddleMaxSpeed : 0,
    ball : null,
    scoreFontSize : 0,

    preferences : {
        COURT_WIDTH : 1600,
        COURT_HEIGHT: 900,
        WALL_THICKNESS_PERCENTAGE : 5,
        PADDLE_HEIGHT_PERCENTAGE : 20,
        PADDLE_THICKNESS_PERCENTAGE : 5,
        PADDLE_WALL_SEPARATION_PERCENTAGE : 5,
        PADDLE_SPEED_PERCENTAGE_PER_SECOND : 50,
        BALL_SIZE_PERCENTAGE : 5,
        BALL_SPEED_PERCENTAGE_PER_SECOND : 50,
        SCORE_FONT_SIZE_PERCENTAGE : 10,
        PADDLE1_COLOR : 'white',
        PADDLE2_COLOR : 'white',
        WALL_COLOR : 'white',
        BALL_COLOR : 'white',
        CLEAR_COLOR : 'black'
    },


    initialize : function(input, renderer) {
        // Initialize the timer
        this.timer = new Pong.Timer();

        // Initialize the frames per second counter
        this.fpsCounter = new Pong.FPSCounter();

        // Get the input
        this.input = input;

        // IMPORTANT: The renderer must be assigned before initializing the input because some input handlers may need access to it.
        this.renderer = renderer;

        // Initialize the input
        this.input.initialize(this);

        // Calculate the factor to convert game space coordinates to render space coordinates.
        this.gameSpaceToRenderSpaceConversionFactor = Math.min(this.renderer.getWidth() / this.preferences.COURT_WIDTH, this.renderer.getHeight() / this.preferences.COURT_HEIGHT);

        // Choose the smallest of the width and height. This value will be use to calculate the final values for some of the percentages included in the preferences.
        var smallest = Math.min(this.preferences.COURT_WIDTH, this.preferences.COURT_HEIGHT);

        // Initialize the walls
        var wallThickness = smallest * this.preferences.WALL_THICKNESS_PERCENTAGE / 100.0;
        this.leftWall = new Pong.Rectangle();
        this.leftWall.initialize(0, 0, wallThickness, this.preferences.COURT_HEIGHT);
        this.rightWall = new Pong.Rectangle();
        this.rightWall.initialize(this.preferences.COURT_WIDTH - wallThickness, 0, wallThickness, this.preferences.COURT_HEIGHT);
        this.topWall = new Pong.Rectangle();
        this.topWall.initialize(0, 0, this.preferences.COURT_WIDTH, wallThickness);
        this.bottomWall = new Pong.Rectangle();
        this.bottomWall.initialize(0, this.preferences.COURT_HEIGHT - wallThickness, this.preferences.COURT_WIDTH, wallThickness);

        // Initialize the paddles.
        var paddleHeight = this.preferences.COURT_HEIGHT * this.preferences.PADDLE_HEIGHT_PERCENTAGE / 100.0;
        var paddleThickness = smallest * this.preferences.PADDLE_THICKNESS_PERCENTAGE / 100.0;
        var paddleWallSeparation = this.preferences.COURT_WIDTH * this.preferences.PADDLE_WALL_SEPARATION_PERCENTAGE / 100.0;
        var paddleCenteredY = (this.preferences.COURT_HEIGHT - paddleHeight) >> 1;
        this.paddleMaxSpeed = smallest * this.preferences.PADDLE_SPEED_PERCENTAGE_PER_SECOND / 100.0;
        this.paddleLeft = new Pong.Paddle();
        this.paddleLeft.initialize(this.leftWall.x + this.leftWall.width + paddleWallSeparation, paddleCenteredY, paddleThickness, paddleHeight);
        this.paddleLeft.scoreX = this.preferences.COURT_WIDTH >> 2;
        this.paddleLeft.scoreY = this.preferences.COURT_HEIGHT >> 3;
        this.paddleRight = new Pong.Paddle();
        this.paddleRight.initialize(this.rightWall.x - paddleWallSeparation - paddleThickness, paddleCenteredY, paddleThickness, paddleHeight);
        this.paddleRight.scoreX = (this.preferences.COURT_WIDTH >> 1) + (this.preferences.COURT_WIDTH >> 2);
        this.paddleRight.scoreY = this.preferences.COURT_HEIGHT >> 3;

        // Initialize the ball
        var ballSize = smallest * this.preferences.BALL_SIZE_PERCENTAGE / 100.0;
        var ballSpeed = smallest * this.preferences.BALL_SPEED_PERCENTAGE_PER_SECOND / 100.0;
        this.ball = new Pong.Ball();
        this.ball.initialize(0, 0, ballSize, ballSize, 0, 0, ballSpeed);
        this.resetBall();

        // Calculate the size of the score font.
        this.scoreFontSize = smallest * this.preferences.SCORE_FONT_SIZE_PERCENTAGE / 100.0;
    },

    finalize : function() {
        input.finalize();
    },

    paddleVsTopBottomWalls : function(paddle) {
        if (paddle.collides(this.topWall) || ((paddle.y + paddle.height) < 0)) {
            paddle.y = this.topWall.height;
        }
        else if (paddle.collides(this.bottomWall) || paddle.y > this.preferences.COURT_HEIGHT) {
            paddle.y = this.bottomWall.y - paddle.height;
        }
    },

    resetBall : function() {
        var ballDirX = Math.random() > 0.5 ? 1 : -1;
        var ballDirY = Math.random() * 2 - 1;
        this.ball.x = (this.preferences.COURT_WIDTH - this.ball.width) >> 1
        this.ball.y = (this.preferences.COURT_HEIGHT - this.ball.height) >> 1;
        this.ball.dirX = ballDirX;
        this.ball.dirY = ballDirY;
    },

    update : function() {
        // Update the timer (so we can get the accumulated and elapsed time)
        this.timer.update();

        // Update the FPS counter
        this.fpsCounter.update();

        // Update the input
        this.input.update();

        // Update the paddles
        this.paddleLeft.update(this.timer.elapsedTimeInSeconds);
        this.paddleRight.update(this.timer.elapsedTimeInSeconds);

        // Update the ball
        this.ball.update(this.timer.elapsedTimeInSeconds);

        // Now check collisions between paddels and walls
        this.paddleVsTopBottomWalls(this.paddleLeft);
        this.paddleVsTopBottomWalls(this.paddleRight);

        // Now check collisions between ball and walls and ball and paddels
        if (this.ball.collides(this.topWall)) {
            this.ball.y = this.topWall.height;
            this.ball.dirY = -this.ball.dirY;
        }
        if (this.ball.collides(this.bottomWall)) {
            this.ball.y = this.bottomWall.y - this.ball.height;
            this.ball.dirY = -this.ball.dirY;
        }
        if (this.ball.collides(this.leftWall)) {
            this.paddleRight.score++;
            this.resetBall();
        }
        if (this.ball.collides(this.rightWall)) {
            this.paddleLeft.score++;
            this.resetBall();
        }
        if (this.ball.collides(this.paddleLeft)) {
            this.ball.x = this.paddleLeft.x + this.paddleLeft.width;
            this.ball.dirX = -this.ball.dirX;
        }
        if (this.ball.collides(this.paddleRight)) {
            this.ball.x = this.paddleRight.x - this.ball.width;
            this.ball.dirX = -this.ball.dirX;
        }

        // Security check for the ball
        if (this.ball.x + this.ball.width < 0 ||
            this.ball.x > this.preferences.COURT_WIDTH ||
            this.ball.y + this.ball.height < 0 ||
            this.ball.y > this.preferences.COURT_HEIGHT) {
            this.resetBall();
        }
    },

    renderRectangle : function(rectangle, color) {
        var x = this.fromGameSpaceToRenderSpace(rectangle.x);
        var y = this.fromGameSpaceToRenderSpace(rectangle.y);
        var width = rectangle.width * this.gameSpaceToRenderSpaceConversionFactor;
        var height = rectangle.height * this.gameSpaceToRenderSpaceConversionFactor;
        this.renderer.renderRectangle(x, y, width, height, color);
    },

    renderText : function(x, y, text, fontSize) {
        x = this.fromGameSpaceToRenderSpace(x);
        y = this.fromGameSpaceToRenderSpace(y);
        fontSize = this.fromGameSpaceToRenderSpace(fontSize);
        this.renderer.renderText(x, y, text, fontSize);
    },

    render : function() {
        this.renderer.beginFrame();
        this.renderer.clear(this.preferences.CLEAR_COLOR);
        this.renderRectangle(this.leftWall, this.preferences.WALL_COLOR);
        this.renderRectangle(this.rightWall, this.preferences.WALL_COLOR);
        this.renderRectangle(this.topWall, this.preferences.WALL_COLOR);
        this.renderRectangle(this.bottomWall, this.preferences.WALL_COLOR);
        this.renderRectangle(this.paddleLeft, this.preferences.PADDLE1_COLOR);
        this.renderRectangle(this.paddleRight, this.preferences.PADDLE2_COLOR);
        this.renderRectangle(this.ball, this.preferences.BALL_COLOR);
        this.renderText(this.paddleLeft.scoreX, this.paddleLeft.scoreY, "" + this.paddleLeft.score, this.scoreFontSize);
        this.renderText(this.paddleRight.scoreX, this.paddleRight.scoreY, "" + this.paddleRight.score, this.scoreFontSize);
        this.renderer.endFrame();
    },

    loop : function() {
        this.update();
        this.render();
    },

    fromGameSpaceToRenderSpace : function(value) {
        return value * this.gameSpaceToRenderSpaceConversionFactor;
    },

    fromRenderSpaceToGameSpace : function(value) {
        return value / this.gameSpaceToRenderSpaceConversionFactor;
    }
};
