/**
* Author: Iker Jamardo Zugaza
* Company: Ludei (www.ludei.com)
* Disclaimer: 
    Please, note that the purpose of this software is merely educational. Plenty
    of error checking should be added to make it for professional use.
* License: LGPL v3
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* =====================
* Pong.Input
* =====================
*/
Pong.Input = function() {
};

Pong.Input.prototype = {
    game : null,

    initialize : function(game) {
        this.game = game;
    },

    finalize : function() {
        this.game = null;
    },

    update : function() {
    },

    setPaddlePosition : function(gameSpaceX, gameSpaceY) {
        // If the position is in the first half, use it to move the left paddle. If it's in the second half, use it to move the right paddle.
        if (gameSpaceX < (this.game.preferences.COURT_WIDTH >> 1)) {
            this.game.paddleLeft.y = gameSpaceY;
        }
        else {
            this.game.paddleRight.y = gameSpaceY;
        }
    },

    setPaddleDirectionAndSpeed : function(paddleIndex, dirY, speed) {
        // First find out what paddle are we talking about ;)
        var paddle = null;
        switch(paddleIndex) {
            case 0:
                paddle = this.game.paddleLeft;
                break;
            case 1:
                paddle = this.game.paddleRight;
                break;
        }
        // Now that we know the paddle, apply the direction and speed
        paddle.dirY = dirY;
        paddle.speed = speed;
    }
};

/**
* =====================
* Pong.InputScreenSpace
* =====================
*/
Pong.InputScreenSpace = function() {
};

Pong.extend(Pong.InputScreenSpace, Pong.Input);

Pong.InputScreenSpace.prototype.setPaddlePosition = function(screenSpaceX, screenSpaceY) {
    var gameSpaceX = this.game.fromRenderSpaceToGameSpace(screenSpaceX);
    var gameSpaceY = this.game.fromRenderSpaceToGameSpace(screenSpaceY);
    Pong.InputScreenSpace.superClass.setPaddlePosition.call(this, gameSpaceX, gameSpaceY);
}

/**
* =====================
* Pong.InputMouse
* =====================
*/
Pong.InputMouse = function() {
};

Pong.extend(Pong.InputMouse, Pong.InputScreenSpace);

Pong.InputMouse.prototype.mouseDown = false;

Pong.InputMouse.prototype.onMouseDown = function(event) {
    var inputMouse = event.target.pong_inputMouse;
    inputMouse.mouseDown = true;
};

Pong.InputMouse.prototype.onMouseUp = function(event) {
    var inputMouse = event.target.pong_inputMouse;
    inputMouse.mouseDown = false;
};

Pong.InputMouse.prototype.onMouseMove = function(event) {
    var inputMouse = event.target.pong_inputMouse;
    if (inputMouse.mouseDown) {
        inputMouse.setPaddlePosition(event.pageX, event.pageY);
    }
};

Pong.InputMouse.prototype.initialize = function(game) {
    Pong.InputMouse.superClass.initialize.call(this, game);

    if (this.game && this.game.renderer && this.game.renderer.canvas) {
        // Store this object in the canvas (the target of the events) to avoid memory leaks
        this.game.renderer.canvas.pong_inputMouse = this;
        this.game.renderer.canvas.addEventListener("mousedown", this.onMouseDown);
        this.game.renderer.canvas.addEventListener("mouseup", this.onMouseUp);
        this.game.renderer.canvas.addEventListener("mousemove", this.onMouseMove);
    }
};

Pong.InputMouse.prototype.finalize = function() {
    if (this.game && this.game.renderer && this.game.renderer.canvas) {
        // Remove the events
        this.game.renderer.canvas.removeEventListener("mousedown", this.onMouseDown);
        this.game.renderer.canvas.removeEventListener("mouseup", this.onMouseUp);
        this.game.renderer.canvas.removeEventListener("mousemove", this.onMouseMove);
        // Remove the reference to this inside the canvas.
        this.game.renderer.canvas.pong_inputMouse = null;
    }
    Pong.InputMouse.superClass.finalize.call(this);
};

/**
* =====================
* Pong.InputTouch
* =====================
*/
Pong.InputTouch = function() {
};

Pong.extend(Pong.InputTouch, Pong.InputScreenSpace);

Pong.InputTouch.prototype.onTouchMove = function(event) {
    event.preventDefault();
    var inputTouch = event.target.pong_inputTouch;
    for (var i = 0; i < event.touches.length; i++) {
        inputTouch.setPaddlePosition(event.touches[i].pageX, event.touches[i].pageY);
    }
};

Pong.InputTouch.prototype.initialize = function(game) {
    Pong.InputTouch.superClass.initialize.call(this, game);

    if (this.game.renderer && this.game.renderer.canvas) {
        // Store this object in the canvas (the target of the events) to avoid memory leaks
        this.game.renderer.canvas.pong_inputTouch = this;
        this.game.renderer.canvas.addEventListener("touchmove", this.onTouchMove);
    }
};

Pong.InputTouch.prototype.finalize = function() {
    if (this.game.renderer && this.game.renderer.canvas) {
        // Remove the events
        this.game.renderer.canvas.removeEventListener("touchmove", this.onTouchMove);
        // Remove the reference to this inside the canvas.
        this.game.renderer.canvas.pong_inputTouch = null;
    }
    Pong.InputTouch.superClass.finalize.call(this);
};

/**
* =====================
* Pong.InputLeap
* =====================
*/
Pong.InputLeap = function() {
};

Pong.extend(Pong.InputLeap, Pong.Input);

Pong.InputLeap.prototype.websocket = null;
Pong.InputLeap.prototype.leapSpaceToGameSpaceConversionFactor = null;
Pong.InputLeap.prototype.LEAP_URL = "ws://10.251.38.100:6437/";
Pong.InputLeap.prototype.LEAP_RADIUS = 400;

/**
* This boolean specifies if hand of finger positions should be used to calculate the paddle positions.
*/
Pong.InputLeap.prototype.USE_HANDS = true;

Pong.InputLeap.prototype.onWebSocketOpen = function(event) {
    var inputLeap = event.target.pong_inputLeap;
    inputLeap.websocket.connected = true;
};

Pong.InputLeap.prototype.onWebSocketMessage = function(event) {
    var inputLeap = event.target.pong_inputLeap;
    var leapData = JSON.parse(event.data);

    // Depending on using hand or finger detection, the array is different
    var leapDataArray = this.USE_HANDS ? leapData.hands : leapData.pointables;

    if (leapData && leapDataArray && leapDataArray.length > 0)
    {
        // If using fingers, this array will ensure only one finger is used per hand.
        var handIds = null;
        if (!this.USE_HANDS) {
            handIds = [];
        }

        // The position attribute name for the hand or for the finger is different.
        var positionAttributeName = this.USE_HANDS ? "palmPosition" : "tipPosition";

        for (var i = 0; i < leapDataArray.length; i++) {
            // If using fingers the hand if the finger has already been used, just continue to check the next finger.
            if (!this.USE_HANDS && handIds.indexOf(leapDataArray[i].handId) >= 0) continue;
            // If the finger is from a hand that has not bees used yet, store the id of the hand so no more fingers of it will be used in future iterations.
            if (!this.USE_HANDS) handIds.push(leapDataArray[i].handId);

            // Get the position (hand or finger)
            var leapSpaceX = leapDataArray[i][positionAttributeName][0];
            var leapSpaceY = leapDataArray[i][positionAttributeName][1];

            // Pass the position from leap space coordinates to game coordinates.
            var gameSpaceX = (inputLeap.game.preferences.COURT_WIDTH >> 1) + leapSpaceX * inputLeap.leapSpaceToGameSpaceConversionFactor;
            var gameSpaceY = inputLeap.game.preferences.COURT_HEIGHT - leapSpaceY * inputLeap.leapSpaceToGameSpaceConversionFactor;

            // Apply the position to the game
            inputLeap.setPaddlePosition(gameSpaceX, gameSpaceY);
        }
    }
};

Pong.InputLeap.prototype.onWebSocketClose = function(event) {
    var inputLeap = event.target.pong_inputLeap;
    if (!inputLeap.websocket.connected) {
        // throw "Could not initialize the connection with the leap motion device.";
    }
    inputLeap.websocket.connected = false;
};

Pong.InputLeap.prototype.onWebSocketError = function(event) {
    // throw "Error while connecting with the leap motion device.";
};

Pong.InputLeap.prototype.initialize = function(game) {
    Pong.InputLeap.superClass.initialize.call(this, game);

    if ((typeof(WebSocket) == 'undefined') &&
        (typeof(MozWebSocket) != 'undefined')) {
        window.WebSocket = MozWebSocket;
    }

    if (typeof(window.WebSocket) == 'undefined') {
        throw "Trying to initialize InputLeap but there is no WebSocket support.";
    }

    this.leapSpaceToGameSpaceConversionFactor = Math.min(this.game.preferences.COURT_WIDTH / this.LEAP_RADIUS, this.game.preferences.COURT_HEIGHT / this.LEAP_RADIUS);

    this.websocket = new WebSocket(this.LEAP_URL);

    this.websocket.pong_inputLeap = this;

    this.websocket.addEventListener("open", this.onWebSocketOpen);
    this.websocket.addEventListener("message", this.onWebSocketMessage);
    this.websocket.addEventListener("close", this.onWebSocketClose);
    this.websocket.addEventListener("error", this.onWebSocketError);
};

Pong.InputLeap.prototype.finalize = function() {
    this.websocket = null;
    Pong.InputLeap.superClass.finalize.call(this);
};

/**
* =====================
* Pong.InputKeyboard
* =====================
*/
Pong.InputKeyboard = function() {
};

Pong.extend(Pong.InputKeyboard, Pong.Input);

Pong.InputKeyboard.prototype.paddleKeyCodes = [ [ 87, 83 ], [ 38, 40 ] ];
Pong.InputKeyboard.prototype.paddleKeyData = { paddleIndex : -1, keyIndex : -1 };

Pong.InputKeyboard.prototype.processEvent = function(event) {
    var result = null;
    for (var paddleIndex = 0; !result && paddleIndex < this.paddleKeyCodes.length; paddleIndex++) {
        for (var keyIndex = 0; !result && keyIndex < this.paddleKeyCodes[paddleIndex].length; keyIndex++) {
            if (event.keyCode === this.paddleKeyCodes[paddleIndex][keyIndex]) {
                result = this.paddleKeyData;
                result.paddleIndex = paddleIndex;
                result.keyIndex = keyIndex;
            }
        }
    }
    return result;
};

Pong.InputKeyboard.prototype.onKeyUp = function(event) {
    var inputKeyboard = window.pong_inputKeyboard;
    // event.preventDefault();
    var result = inputKeyboard.processEvent(event || window.event);
    if (result) {
        inputKeyboard.setPaddleDirectionAndSpeed(result.paddleIndex, 0, 0);
    }
};

Pong.InputKeyboard.prototype.onKeyDown = function(event) {
    var inputKeyboard = window.pong_inputKeyboard;
    // event.preventDefault();
    var result = inputKeyboard.processEvent(event || window.event);
    if (result) {
        inputKeyboard.setPaddleDirectionAndSpeed(result.paddleIndex, result.keyIndex === 0 ? -1 : 1, inputKeyboard.game.paddleMaxSpeed);
    }
};

Pong.InputKeyboard.prototype.initialize = function(game) {
    Pong.InputKeyboard.superClass.initialize.call(this, game);
    window.pong_inputKeyboard = this;
    window.addEventListener("keydown", this.onKeyDown);
    window.addEventListener("keyup", this.onKeyUp);
};

Pong.InputKeyboard.prototype.finalize = function() {
    window.removeEventListener("keydown", this.onKeyDown);
    window.removeEventListener("keyup", this.onKeyUp);
    window.pong_inputKeyboard = null;
    Pong.InputKeyboard.superClass.finalize.call(this);
};

/**
* =====================
* Pong.InputGamepads
* =====================
*/
Pong.InputGamepads = function() {
};

Pong.extend(Pong.InputGamepads, Pong.Input);

Pong.InputGamepads.prototype.leftHanded = [false, false];
Pong.InputGamepads.prototype.paddleYValues = [0, 0];
Pong.InputGamepads.prototype.oldPaddleYValues = [0, 0];
Pong.InputGamepads.prototype.AXE_DEAD_ZONE = 0.20;

Pong.InputGamepads.prototype.getNumberOfValidGamepads = function(gamepads) {
    var numberOfValidGamepads = 0;
    for (var i = 0; i < gamepads.length; i++) {
        if (gamepads[i]) {
            numberOfValidGamepads++;
        }
    }
    return numberOfValidGamepads;
};

Pong.InputGamepads.prototype.initialize = function(game) {
    Pong.InputGamepads.superClass.initialize.call(this, game);
    if (!navigator.getGamepads) {
        throw "There is no gamepad API in the system.";
    }
};

Pong.InputGamepads.prototype.update = function() {
    var gamepads = navigator.getGamepads();
    var numberOfValidGamepads = this.getNumberOfValidGamepads(gamepads);
    if (numberOfValidGamepads >= 1)
    {
        this.paddleYValues[0] = this.paddleYValues[1] = 0;
        if (numberOfValidGamepads === 1) {
            // Only onw gamepad? Allow to move both paddles using both analog joysticks
            this.paddleYValues[0] = gamepads[0].axes[1];
            this.paddleYValues[1] = gamepads[0].axes[3];
        }
        else {
            // If there are more than one gamepad, allow left or right analig sticks to control the paddles.
            this.paddleYValues[0] = gamepads[0].axes[this.leftHanded[0] ? 1 : 3];
            this.paddleYValues[1] = gamepads[1].axes[this.leftHanded[1] ? 1 : 3];
        }

        for (var i = 0; i < this.paddleYValues.length; i++) {
            var apply = false;
            // Only if the axe has passed a threshold is considered an applicable value
            if (Math.abs(this.paddleYValues[i]) >= this.AXE_DEAD_ZONE) {
                apply = true;
            }
            // If the previous value passed the threshold, then apply zero.
            else if (Math.abs(this.oldPaddleYValues[i]) > 0) {
                apply = true;
                this.paddleYValues[i] = 0;
            }
            if (apply) {
                this.setPaddleDirectionAndSpeed(i, this.paddleYValues[i], this.game.paddleMaxSpeed);
                // Only update the last value when it is applied this way the gamepad does not override other input mechanisms.
                this.oldPaddleYValues[i] = this.paddleYValues[i];
            }
        }
    }
};

/**
* =====================
* Pong.InputContainer
* =====================
*/
Pong.InputContainer = function() {
};

Pong.extend(Pong.InputContainer, Pong.Input);

Pong.InputContainer.prototype.inputs = [];

Pong.InputContainer.prototype.initialize = function(game) {
    for (var i = 0; i < this.inputs.length; i++) {
        try {
            this.inputs[i].initialize(game);
        }
        catch(e) {
        }
    }
};

Pong.InputContainer.prototype.update = function() {
    for (var i = 0; i < this.inputs.length; i++) {
        try {
            this.inputs[i].update();
        }
        catch(e) {
        }
    }
};

Pong.InputContainer.prototype.finalize = function() {
    for (var i = 0; i < this.inputs.length; i++) {
        try {
            this.inputs[i].finalize();
        }
        catch(e) {
        }
    }
};

Pong.InputContainer.prototype.addInput = function(input) {
    if (this.inputs.indexOf(input) < 0) {
        this.inputs.push(input);
    }
};

Pong.InputContainer.prototype.removeInput = function(input) {
    var index = this.inputs.indexOf(input);
    if (index >= 0) {
        this.inputs = this.inputs.splice(index, 1);
    }
};
